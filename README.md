# EOS Icon picker

API and Temporary front end for EOS-Icons-Picker project

#Installation

`git clone https://gitlab.com/SUSE-UIUX/eos-icon-picker.git`

Once the repository is cloned, run `npm i` to install the required packages to run this project.

#Running the project

To start the server, run `npm start` and your server will be started. Visit `http://localhost:3000/` to see the site.

#Selecting icons

On the home page, you can select icons by clicking on them. Once all the required icons are selected, click on Generate font. The API and grunt will process the request and after successful completion a folder with name `dist{ts}` will be created in the main directory (Where `ts` is timestamp of the time when API request was made).

This folder will have all the required files and the iconic fonts only with selected icons. You can also verify if API request was successful by checking the console in the browser. The response would be a full grunt command used to generate the font.

# Importing JSON and continuing building previous fonts

When you create an iconic font, an `icons_config.json` file is also created with all the information related to the icons. You can also continue building that same font by clicking the `Continue building a previous font` link on the home page. There you can upload the `icons_config.json` file. Once it's uploaded, your previous selection will get active and you can select more icons and follow the same process to Generate the font again.

# Downloading generated files

After clicking Generate button or Successful API request the timestamp (`ts`) is returned by the API.It also appear in the browser's console. In order to download the zip file with iconic font you can use the following URL: http://localhost:3000/download?ts={ts} (`ts` being timestamp).

# Learn more about the EOS Design System

* [EOS Design System](https://www.eosdesignsystem.com/)

* [EOS Icons](icons.eosdesignsystem.com/)

* [Follow us on Twitter](https://twitter.com/eosdesignsystem)

* [Join us in Slack](https://slack.eosdesignsystem.com)

# Our "thank you" section

### Tested for every browser in every device

Thanks to [Browserstack](https://www.browserstack.com) and their continuous contribution to open source projects, we continuously test the EOS to make sure all our features and components work perfectly fine in all browsers.
Browserstack helps us make sure our Design System also delivers a peace of mind to all developers and designers making use of our components and layout in their products.
